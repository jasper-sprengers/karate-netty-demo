Feature: Reference implementation

  Background:
    # cors setting allows you to run the mock server locally under a different port than the web application
    * configure cors = true
    # use a fixed delay of 50 milliseconds before responding
    * def responseDelay = 50

     # this more generic example triggers for any get to the /friends endpoint, and therefore should be defined after the more specific previous case friends?age=50 in the other file
  Scenario: pathMatches('/friends') && methodIs('get')
    * def response = read('friends.json')

  Scenario: pathMatches('/friends/1') && methodIs('get')
    * def response = {id: 1, "name": "John"}

    # You can also do detailed matching on uploaded files, and thus simulate validation flows by uploading empty files with different names
  Scenario: pathMatches('/v1/image/upload') && requestParts['photo'][0].name == 'valid.jpg'
    * def responseDelay = 1000
    * def response = "ACCEPTED"
