Feature: Alternative scenarios

  Background:
    # cors setting allows you to run the mock server locally under a different port than the web application
    * configure cors = true
    # use a fixed delay of 50 milliseconds before responding
    * def responseDelay = 50

    # you can omit the methodIs portion if the url for pathMatches is unique in the configuration. It will then match any method
  Scenario: pathMatches('/friends/2')
    * def response = {id: 2, "name": "Holly"}

  # It's good practice to include at least one scenario that triggers a server exception
  Scenario: pathMatches('/friends/666')
    * def responseCode = 500

  # this scenario triggers when the url contains an age request parameter /friends?age=30.
  Scenario: pathMatches('/friends') && paramValue('age') == 30 && methodIs('get')
    * def response = [{id: 1, "name": "John"}]

    # Note how you can match on a value in the request payload. E.g. {"married": true, "maxResults": 5}
  Scenario: pathMatches('/friends/search') && request.married == false > 20 && methodIs('post')
    * def response = [{id: 2, "name": "Holly"}]

  Scenario: pathMatches('/v1/image/upload') && requestParts['photo'][0].name == 'invalid.jpg'
    * def responseDelay = 4000
    * def response = "REJECTED"
    * def responseCode = 400
