# Karate Netty demo

This is a companion project to the Java Magazine article about the Karate Netty mock server.

## Useful links to the official project

* Oficial documentation of the [Karate Netty project](https://karatelabs.github.io/karate/karate-netty/)
* [Standalone 1.3.0 server executable](https://github.com/karatelabs/karate/releases/download/v1.3.0/karate-1.3.0.jar). Requires a Java runtime. See the official documentation for other installation options 
* [Latest releases](https://github.com/karatelabs/karate/releases).

## Running the example
Download the latest karate executable to the `karate` folder. Rename it to `karate.jar`, or update the reference accordingly in the `run.sh` script.

```bash
cd karate
./run.sh
```

This will start a local server at port 8090. Use your browser or any REST client to play around with the examples. Feel free to add your own.
